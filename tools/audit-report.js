'use strict'

const fs = require('fs')
const audit = require('../gl-dependency-scanning-report.json')
const listofValues = Object.values(audit.vulnerabilities)
const result = []


function getPriority(priority) {
  switch (priority.toLowerCase()) {
    case 'critical':
      return 'BLOCKER'
    case 'high':
      return 'CRITICAL'
    case 'medium':
      return 'MAJOR'
    case 'low':
      return 'MINOR'
    default:
      return 'INFO'
  }
}

for (const obj of listofValues) {
  var prilocation={}
  var textRange={}

var seclocation=[]

  var location = obj.location

   textRange['startLine']=1
  textRange['endLine']=10
  textRange['startColumn']=1
  textRange['endColumn']=10

  prilocation['message'] = obj.message + ' - '+ obj.cve
  prilocation['filePath'] = location.file
  prilocation['textRange'] = textRange
 
  seclocation.push({
   message: ''
  ,filePath:''
  ,textRange :textRange
  })
  result.push({
    engineId:obj.category
  ,  ruleId: obj.id
  , severity: getPriority(obj.severity)
  , type: 'VULNERABILITY'
  , primaryLocation: prilocation
  , effortMinutes : 10
  , secondaryLocations: seclocation
  })

}
var issues={}
issues['issues']=result
const filename = './coverage/external_audit.json'
var jsonStr = JSON.stringify(issues)
console.log(jsonStr)
var jsonVal = JSON.parse(jsonStr)
console.log(jsonVal)

fs.writeFile (filename, JSON.stringify(jsonVal), function(err) {
    if (err) throw err;
    console.log('complete json conversion');
    }
);
